const validate = require('validator');

const res = validate.isLength('s', {max:10});

// console.log(res)


class A {
    constructor(){
        this.a = 'a'
    }
}

class B extends A{
    constructor(){
        super();
        this.b = 'b'
    }
}

class C extends B{
    constructor(){
        super();
        this.c = 'C'
    }
}
const a = new A();
const b = new B();
const c = new C();

console.log(a instanceof A);
console.log(b instanceof A);
console.log(c instanceof A);
